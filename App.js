import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
import { WebView } from 'react-native-webview';
// import {  } from 'react-native';

export default function App() {

  let isLoading = true;

  return (
    <>
    {isLoading ? (
      <WebView
          source={{ uri: 'https://ubryng-app.bubbleapps.io/version-test' }}
          style={styles.container}
        
      />
    ) : (

      <View style={styles.container}>
        <Text>Hello World</Text>
      </View>

    ) }
    
    </>
  );
}



// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     marginTop: Constants.statusBarHeight,
//   },
// });


const screenHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop:40,
    maxHeight:screenHeight
  },
});
